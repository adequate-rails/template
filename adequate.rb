# Adequate Modern Rails Application Setup

repository = { app: 'git@gitlab.com:adequate-rails/app.git',
               api: 'git@gitlab.com:adequate-rails/api.git' }

app_name = `echo $PWD`.split('/').last.strip

git :init

question = <<~EOT

############################################################
# Please specify application type.
#
# 1: Full application
# 2: Api only application
############################################################

EOT

loop do
  answer = ask(question)

  case answer
  when '1'
    @type = :app
    break
  when '2'
    @type = :api
    break
  else
    next
  end
end

def api?
  @type == :api
end

def app?
  @type == :app
end

puts <<~EOS

##############################
# Cloning #{@type} files...
##############################

EOS

path = "https://gitlab.com/adequate-rails/#{@type.to_s}/raw/master"

# Get Gemfile
run "curl -L #{path}/Gemfile > Gemfile"

# Get Readme.md
run "curl -L #{path}/README.md > README.md"

# Get Rakefile
run "curl -L #{path}/Rakefile > Rakefile"

# Get application.js
run "curl -L #{path}/app/assets/javascripts/application.js > app/assets/javascripts/application.js" if app?

# Get application controller
run "curl -L #{path}/app/controllers/application_controller.rb > app/controllers/application_controller.rb"

# Get application helper
run "curl -L #{path}/app/helpers/application_helper.rb > app/helpers/application_helper.rb" if app?

# Get application job
run "curl -L #{path}/app/jobs/application_job.rb > app/jobs/application_job.rb"

# Get application mailer
run "curl -L #{path}/app/mailers/application_mailer.rb > app/mailers/application_mailer.rb"

# Get application record
run "curl -L #{path}/app/models/application_record.rb > app/models/application_record.rb"

# Get application template
run "curl -L #{path}/app/views/layouts/application.html.erb > app/views/layouts/application.html.erb" if app?

puts 'Death is inevitable'

# Get .rubocop.yml
run "curl -L #{path}/.rubocop.yml > .rubocop.yml"

# Get Guardfile
run "curl -L #{path}/Guardfile > Guardfile"

# Get .gitlab-ci.yml
run "curl -L #{path}/.gitlab-ci.yml > .gitlab-ci.yml"

# Get database.yml
run "curl -L #{path}/config/database.yml > config/database.yml"

# Update database name with application's name
gsub_file('config/database.yml', app? ? 'adequate-app' : 'adequate-api', app_name)

# Initialize schema
run "curl -L #{path}/db/schema.rb > db/schema.rb"

# Get application controller test
run "curl -L #{path}/test/controllers/application_controller_test.rb > test/controllers/application_controller_test.rb"
run "rm test/controllers/.keep"

# Get application job test
run "mkdir test/jobs"
run "curl -L #{path}/test/jobs/application_job_test.rb > test/jobs/application_job_test.rb"

# Get application record test
run "curl -L #{path}/test/models/application_record_test.rb > test/models/application_record_test.rb" if app?
run "rm test/models/.keep" if app?

# Get test helper
run "curl -L #{path}/test/test_helper.rb > test/test_helper.rb"


puts <<~EOS

##############################
# Necessary files cloned!
##############################

EOS

question = <<~EOT

############################################################
# Do you want to add deploy configuration for Gitlab CI ?
# 1: Why not
# 2: No thanks
# 3: What is that mean?
############################################################

EOT

loop do
  answer = ask(question)

  case answer
  when '1'
    # Get gitlab-ci.yml.dokku
    run "curl -L #{path}/.gitlab-ci.dokku.yml > .gitlab-ci.yml"
    puts <<~EOS

    ##############################
    Successfully cloned!
    ##############################

    EOS
    break
  when '3'
    text = <<~EOS

      ##################################################################################################
      # With this configuration, your application automatically will be deployed to your dokku server. #
      # * You need to set SSH_KEY project variable in your Gitlab repo.                                #
      ##################################################################################################

    EOS
    puts text
    just = ask('Press any key to continue...')
    next
  else
    break
  end
end

puts <<~EOS

############################################################
# Creating database and loading schema.
############################################################

EOS

rails_command 'db:drop db:setup'

after_bundle do
  git :status
  run 'bundle exec guard'
end
